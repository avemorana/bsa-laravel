<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Container\Container;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/products/cheap', function () {

    $container = Container::getInstance();
    $action = $container->app->make(\App\Action\Product\GetCheapestProductsAction::class);
    $products = \App\Http\Presenter\ProductArrayPresenter::presentCollection($action->execute()->getProducts());


    return Response::view('cheap_products',[
        'products' => $products
    ]);
});
