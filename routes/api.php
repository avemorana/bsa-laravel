<?php

use Illuminate\Http\Request;
use Illuminate\Container\Container;
use App\Action\Product\GetAllProductsAction;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/products', function () {

    $container = Container::getInstance();
    $action = $container->app->make(\App\Action\Product\GetAllProductsAction::class);
    $products = $action->execute()->getProducts();

    $json = json_encode(\App\Http\Presenter\ProductArrayPresenter::presentCollection($products));
    return Response($json, 200)->header('Content-Type', 'application/json');
});

Route::get('/products/popular', function () {

    $container = Container::getInstance();
    $action = $container->app->make(\App\Action\Product\GetMostPopularProductAction::class);
    $product = $action->execute()->getProduct();

    $json = json_encode(\App\Http\Presenter\ProductArrayPresenter::present($product));
    return Response($json, 200)->header('Content-Type', 'application/json');
});