<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Cheap products</title>
</head>
<body>
List of cheap products
<table>
    <th>id</th>
    <th>name</th>
    <th>price</th>
    <th>url</th>
    <th>rating</th>
    <tbody>
    @foreach($products as $product)
        <tr>
            <td>{{$product['id']}}</td>
            <td>{{$product['name']}}</td>
            <td>{{$product['price']}}</td>
            <td>{{$product['img']}}</td>
            <td>{{$product['rating']}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>