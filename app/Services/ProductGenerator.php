<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\Product;

class ProductGenerator
{
    /**
     * @return Product[]
     */
    public static function generate(): array
    {
        return [
            new Product(
                12,
                'phone',
                18.40,
                'img_phone',
                3.2
            ),
            new Product(
                56,
                'laptop',
                56.12,
                'img_laptop',
                4.8
            ),
            new Product(
                34,
                'desktop',
                70.50,
                'img_desktop',
                3.1
            ),
            new Product(
                73,
                'phone2',
                13.13,
                'img_phone2',
                2.5
            )
        ];
    }
}