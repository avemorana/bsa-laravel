<?php

namespace App\Providers;

use App\Repository\ProductRepository;
use App\Repository\ProductRepositoryInterface;
use App\Services\ProductGenerator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ProductRepositoryInterface::class,
            function ($app) {
                return new ProductRepository(ProductGenerator::generate());
            }
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
