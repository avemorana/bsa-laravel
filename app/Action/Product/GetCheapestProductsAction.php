<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Entity\Product;
use App\Repository\ProductRepositoryInterface;
use Illuminate\Container\Container;

class GetCheapestProductsAction
{
    public function execute(): GetCheapestProductsResponse
    {
        $container = Container::getInstance();
        $repository = $container->app->make(ProductRepositoryInterface::class);
        $products = $repository->findAll();
        usort($products, function (Product $a, Product $b){
            if ($a->getPrice() == $b->getPrice()){
                return 0;
            }
            return ($a->getPrice() < $b->getPrice()) ? -1 : 1;
        });
        $cheapestProducts = array_slice($products, 0, 3);
        return new GetCheapestProductsResponse($cheapestProducts);

    }
}