<?php

declare(strict_types=1);

namespace App\Action\Product;

class GetAllProductsResponse
{
    private $products;

    public function __construct($products)
    {
        $this->products = $products;
    }

    public function getProducts()
    {
        return $this->products;
    }

    public function setProducts($products): void
    {
        $this->products = $products;
    }

}