<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Entity\Product;
use App\Repository\ProductRepositoryInterface;
use Illuminate\Container\Container;

class GetMostPopularProductAction
{

    public function execute(): GetMostPopularProductResponse
    {
        $container = Container::getInstance();
        $repository = $container->app->make(ProductRepositoryInterface::class);
        $products = $repository->findAll();
        usort($products, function (Product $a, Product $b){
            if ($a->getRating() == $b->getRating()){
                return 0;
            }
            return ($a->getPrice() > $b->getPrice()) ? -1 : 1;
        });
        $mostPopularProduct = array_shift($products);
        return new GetMostPopularProductResponse($mostPopularProduct);
    }
}