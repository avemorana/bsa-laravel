<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Action\Product\GetAllProductsResponse;
use App\Repository\ProductRepository;
use App\Repository\ProductRepositoryInterface;
use Illuminate\Container\Container;

class GetAllProductsAction
{
    public function execute(): GetAllProductsResponse
    {
        $container = Container::getInstance();
        $repository = $container->app->make(ProductRepositoryInterface::class);
        $products = $repository->findAll();
        return new GetAllProductsResponse($products);
    }
}