<?php

declare(strict_types=1);

namespace App\Action\Product;

class GetMostPopularProductResponse
{
    private $product;

    public function __construct($product)
    {
        $this->product = $product;
    }

    public function getProduct()
    {
        return $this->product;
    }

    public function setProduct($product): void
    {
        $this->product = $product;
    }}