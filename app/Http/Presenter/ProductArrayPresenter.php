<?php

declare(strict_types=1);

namespace App\Http\Presenter;

use App\Entity\Product;

class ProductArrayPresenter
{
    /**
     * @param Product[] $products
     * @return array
     */
    public static function presentCollection(array $products): array
    {
        $productsArray = array();
        foreach ($products as $product) {
            $productsArray[] = array(
                'id' => $product->getId(),
                'name' => $product->getName(),
                'img' => $product->getImageUrl(),
                'price' => $product->getPrice(),
                'rating' => $product->getRating()
            );
        }
        return $productsArray;
    }

    public static function present(Product $product): array
    {
        return array(
            'id' => $product->getId(),
            'name' => $product->getName(),
            'img' => $product->getImageUrl(),
            'price' => $product->getPrice(),
            'rating' => $product->getRating()
        );
    }
}
